# Python for ATQC #

## Agenda ##

1. Source Control: working with GIT. 
2. Introduction to Python. Environment and Development Tools.
3. OOP: Classes. Interfaces. Polymorphism. 
4. Arrays and Collections in Python.
5. Working with strings. Regular Expression.
6. Debugging of Code.
7. Unit and Integration Testing.
8. HTML5/CSS3 fundamental 
9. JavaScript fundamental
10. XML/XPAth intro
11. Automation Testing. Selenium. Webdriver Base Classes. Selectors.
12. Explicit and Implicit Wait. Testing Ajax Applications.
13. Page-Object Approach. User Repository. Multi Layer Architecture for Automation Tests.
